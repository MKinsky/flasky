from flask import Flask, jsonify, request
import os


app = Flask(__name__)
basedir = os.path.abspath(os.path.dirname(__file__))


@app.route('/')
def home():
    return 'My Flasky Home Page - Welcome'


if __name__ == '__main__':

    app.run()